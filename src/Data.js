import { FaHtml5, FaCss3, FaReact, FaDatabase, FaGitAlt, FaJava ,  FaUser, FaMapMarkerAlt, FaPaperPlane, FaLinkedin, FaFacebook } from "react-icons/fa";
import { DiJavascript1 } from "react-icons/di";
import { SiPolymerproject, SiTailwindcss } from "react-icons/si";
import { SlEnergy } from "react-icons/sl";
import { FiGitlab } from "react-icons/fi"
import { TbBrandNextjs } from "react-icons/tb"
import { GiTeamIdea, GiInnerSelf, GiSpellBook, GiThreeFriends, GiGears} from "react-icons/gi";
import project1 from "./assets/projet1.png";
import project2 from "./assets/projet2.png";
import project3 from "./assets/projet3.png";
import project4 from "./assets/projet4.png";
import project5 from "./assets/projet5.png";
import project6 from "./assets/projet6.png";
import project7 from "./assets/projet7.png";
import project8 from "./assets/projet8.png";
import project9 from "./assets/project9.png"; 
import project10 from "./assets/projet10.png";
import project11 from "./assets/projet11.png";
import project12 from "./assets/projet12d.png";
import project13 from "./assets/projet13.png";
import project14 from "./assets/projet14.png";


export const navLinks = ["accueil", "a propos", "parcours", "competences", "projets", "contact"]

export const socialIcons = [
  {
    icon : <FaLinkedin />,
    link : "https://www.linkedin.com/in/renan-declercq/"
  },
  {
    icon :   <FaFacebook />,
    link : "https://www.facebook.com/Renan.Declercq"
  },
  {
    icon : <FiGitlab />,
    link : "https://gitlab.com/renan110"
  }
]

export const bios = [
  {
    id: 1,
    icon: <FaUser />,
    key: "",
    value: "Declercq Renan",
  },
  {
    id: 2,
    icon: <FaPaperPlane />,
    key: "",
    value: "renan.declercq.etu@univ-lille.fr",
  }
]

export const formations = [
  {
    id : 1,
    year : "2021 - 2024" , 
    label : "BUT Informatique - 3ème année",
    location : "IUT Villeneuve d'Ascq"
  },
  {
    id : 2,
    year : "2023" , 
    label : "Auto-formation Udemy Développement Front-End ",
    location : "HTML - CSS - JavaScript - React"
  },
  {
    id : 3,
    year : "2016 - 2019" , 
    label : "Licence STAPS Management du Sport",
    location : "Université STAPS Lille"
  },
  {
    id : 4,
    year : "2016" , 
    label : "Baccalauréat Scientifique",
    location : "Lycée St Jude Armentières"
  }
]

export const experiences = [
  {
    id : 3,
    year : "2023",
    label : "Stage - Développeur front-end",
    // informations : "Plateforme de géolocalisation et de suivi de flotte de véhicules d'entreprise : Insitu",
    technologies : "Technologie : Polymer JS",
    location : "SituAction , Lesquin"
  },
  {
    id : 2,
    year : "2020 - 2021",
    label : "Aide technique en laboratoire d'analyses médicales",
    location : "Cerballiance , Lille"
  },
  {
    id : 1,
    year : "2019",
    label : "Vendeur",
    location : "Decathlon, Campus Villeneuve d'Ascq - Englos"
  }
]

export const langages = [
  {
    id : 1,
    icon : <DiJavascript1 />,
    label : "JavaScript"
  },
  {
    id : 2,
    icon : <FaReact />,
    label : "React"
  },
  {
    id : 3,
    icon : <TbBrandNextjs />,
    label : "Next JS"
  },
  {
    id : 4,
    icon : <SiPolymerproject />,
    label : "Polymer JS"
  },
  {
    id : 5,
    icon : <FaJava />,
    label : "Java"
  },
  {
    id : 6,
    icon : <FaHtml5 />,
    label : "HTML"
  },
  {
    id : 7,
    icon : <FaCss3 />,
    label : "CSS"
  },
  {
    id : 8,
    icon : <SiTailwindcss />,
    label : "Tailwind CSS"
  },
  {
    id : 9,
    icon : <FaDatabase />,
    label : "SQL"
  },
  {
    id : 10,
    icon : <FaGitAlt />,
    label : "Git"
  }
]

export const transversales = [
  {
    id: 1,
    icon : <GiTeamIdea />,
    transversale: "Travail d'équipe",
  },
  {
    id: 2,
    icon : <GiInnerSelf />,
    transversale: "Autonomie",
  },
  {
    id: 3,
    icon : <GiSpellBook />,
    transversale: "Curiosité",
  },
  {
    id: 4,
    icon : <GiThreeFriends />,
    transversale: "Aisance relationnelle",
  },
  {
    id: 5,
    icon : <SlEnergy />,
    transversale: "Dynamique",
  },
  {
    id: 6,
    icon : <GiGears />,
    transversale: "Adaptabilité",
  },
]

export const workImages = [
  {
    id: 14,
    img: project14,
    name: "2023 : Application web sur la nutrition (En cours)",
    category: "Web",
    langages: "NextJS - React - JavaScript - Tailwind CSS - API openFoodFacts - i18n (multi-langage) - Redux",
    link : "https://gitlab.com/renan110/application-de-nutrition-nextjs-version",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 13,
    img: project13,
    name: "2023 : Mini projet To do list",
    category: "Web",
    langages: "React - JavaScript - HTML - CSS - Bulma",
    link : "https://gitlab.com/renan110/to-do-list",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 12,
    img: project12,
    name: "Stage 2023 : InSitu : Application SaaS de géolocalisation et de suivi de flotte de véhicules d'entreprise ",
    category: "Web",
    langages: "Développement front-end : PolymerJS - HTML - CSS - JavaScript",
    link : "",
    target : "_blank",
    site : "https://www.situaction.fr/",
    displayFirefoxIcon : "",
    displayGitIcon : "none"
  },
  {
    id: 11,
    img: project11,
    name: "2023 : Application web - jeux similaire à agar.io",
    category: "Web",
    langages: "TypeScript - Node.js - Socket.io - Jest - Canvas",
    link : "https://gitlab.com/renan110/agario",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 10,
    img: project10,
    name: "2023 : API REST d'une pizzeria",
    category: "Web",
    langages: "Java EE - SQL - REST - Back-end - Tomcat - Postman - JWT web token",
    link : "https://gitlab.com/renan110/api-rest-pizzeria",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 9,
    img: project9,
    name: "2023 : Déploiement d'une application Matrix-synapse / Element web",
    category: "Web",
    langages: "Nginx - Linux - Procédures Markdown/HTML - VSCode - VirtualBox - SSH",
    link : "https://gitlab.com/renan110/procedures-de-deploiement-matrix-synapse",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 8,
    img: project8,
    name: "2022 : Application de classification de données",
    category: "Logiciel",
    langages: "Java - JavaFX - IA k-NN - Maven - JUnit 5 - MVC - SonarQUBE",
    link : "https://gitlab.com/renan110/application-de-classification-de-donnees",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 7,
    img: project7,
    name: "2022 : Royal Crous Casino",
    category: "Logiciel",
    langages: "Java - Projet agile SCRUM (3 jours pour coder un jeu)",
    link : "https://gitlab.com/renan110/mini-projet-agile-royalcrous",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 6,
    img: project6,
    name: "2022 : Application de gestion de tutorat",
    category: "Logiciel",
    langages: "Java - JavaFX - Graphe - SonarQUBE",
    link : "https://gitlab.com/renan110/gestion-tutorat-java",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  },
  {
    id: 5,
    img: project5,
    name: "2022 : Escape game web",
    category: "Web",
    langages: "HTML - CSS - JavaScript - jQuery",
    link : "https://gitlab.com/renan110/escape-game-web-bio-hazard",
    target : "_blank",
    site : "https://escape-game-iut.netlify.app/",
    displayFirefoxIcon : "",
    displayGitIcon : ""
  },
  {
    id: 4,
    img: project4,
    name: "2021 : Site vitrine Service de mobilité Université de Lille",
    category: "Web",
    langages: "HTML - CSS",
    link : "#projets",
    target : "",
    displayFirefoxIcon : "none",
    displayGitIcon : "none"
  },
  {
    id: 3,
    img: project3,
    name: "2021 : Jeu ludo-pédagogique : Island Escape",
    category: "Logiciel",
    langages: "iJava (Java simplifié)",
    link : "#projets",
    target : "",
    displayFirefoxIcon : "none",
    displayGitIcon : "none"
  },
  {
    id: 2,
    img: project2,
    name: "2021 : Site vitrine IlluStart ",
    category: "Web",
    langages: "HTML - CSS",
    link : "#projets",
    target : "",
    displayFirefoxIcon : "none",
    displayGitIcon : "none"
  },
  {
    id: 1,
    img: project1,
    name: "2021 : Galactik2D : Jeu de plateforme 2D",
    category: "Logiciel",
    langages: "Java - Java Swing",
    link : "https://gitlab.com/renan110/jeu-de-plateforme-galactik2d",
    target : "_blank",
    displayFirefoxIcon : "none",
    displayGitIcon : ""
  }
]


export const workNavs = [
  "Tous les projets", "Web", "Logiciel"
]

export const contacts = [
  {
    id: 1,
    icon: <FaMapMarkerAlt />,
    infoText: "France, Nord , 59000 Lille"
  },
  {
    id: 2,
    icon: <FaPaperPlane />,
    infoText: "renan.declercq.etu@univ-lille.fr"
  }
]
