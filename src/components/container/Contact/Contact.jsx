import React from 'react';
import "./Contact.scss";
import { contacts } from '../../../Data';
import { socialIcons } from '../../../Data';
import { motion } from 'framer-motion';
import emailjs from "emailjs-com";

const Contact = () => {

  function sendEmail(e){
    e.preventDefault();
    emailjs.sendForm('service_tme9qes', 'template_e9bvelq',e.target, 'jbRKk0Q8TNpwV_bYb')
    .then(res => {
      console.log(res);
      window.confirm("Message envoyé !");
    }).catch(error => {
      console.log(error);
      window.confirm("Erreur - message non envoyé ! \n" + error);
    });
  }

  return (
    <div className="container" id="contact">
      <motion.div
        initial={{ opacity: 0 }}
        whileInView={{ y: [-50, 0], opacity: 1 }}
        className="title"
      >
        <span>Entrez en contact</span>
        <h1>Contact</h1>
      </motion.div>
      <div className="contact_form">
        <motion.div
          initial={{ x: 0, opacity: 0 }}
          whileInView={{ x: [-150, 0], opacity: 1 }}
          transition={{ duration: 1 }}
          className='contact_left_container'>
          {contacts.map(contact => {
            return (
              <div className='contact_left' key={contact.id}>
                <div className="icon">
                  {contact.icon}
                </div>
                <p>{contact.infoText}</p>
              </div>
            )
          })}
          <div className="social_icons">
            {socialIcons.map((socialIcon, index) => {
              return (
                <div key={index} >
                  <a href={socialIcon.link} target="_blank" rel="noreferrer noopener">{socialIcon.icon}</a>
                </div>
              )
            })}
          </div>
        </motion.div>
        <motion.div
          initial={{ x: 0, opacity: 0 }}
          whileInView={{ x: [150, 0], opacity: 1 }}
          transition={{ duration: 1 }}
          className="contact_right"

        >
          {/* <form name="contact" method="POST" className="contact_form" data-netlify="true" onSubmit="submit"> */}
          <form name="contact" method="POST" className="contact_form" data-netlify="true" onSubmit={sendEmail}> 
            <input type="hidden" name="form-name" value="contact" />
            <div className="row">
              <input type="text" placeholder="Prenom" name="prenom" />
              <input type="text" placeholder="Nom" name="nom" />
            </div>
            <div className="row">
              <input type="text" placeholder="Telephone" name="telephone" />
              <input type="email" placeholder="Email" name="email" />
            </div>
            <div className="row">
              <textarea placeholder="Message" name="message"></textarea>
            </div>
            <button
              whileHover={{ scale: 1.1 }}
              transition={{ duration: 0.3 }}
              className="btn"
              type="submit"
            >
              Envoyer
            </button>
          </form>
        </motion.div>
      </div>
    </div>
  )
}

export default Contact