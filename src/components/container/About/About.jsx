import React from 'react'
import "./About.scss";
import { motion } from 'framer-motion';
import { bios } from '../../../Data';
import portfolio from "../../../assets/portfolio.jpg";
import photo from "../../../assets/photo_about.png";
const About = () => {


  return (
    <div className="container " id='a propos de moi'>
      <motion.div
        initial={{ opacity: 0 }}
        whileInView={{ y: [-50, 0], opacity: 1 }}
        className="title">
        <span>A propos de moi</span>
        <h1>Présentation</h1>
      </motion.div>

      <div className="about_container">
        <motion.div
          initial={{ x: 0, opacity: 0 }}
          whileInView={{ x: [-250, 0], opacity: 1 }}
          transition={{ duration: 1 }}
          className="about_left">
          <div className="line"></div>
          <motion.img src={photo}
            transition={{ duration: 0.3 }}
          />
          {bios.map(bio => {
            return (
              <div className="bio" key={bio.id}>
                <span className='bioKey'>{bio.icon}{bio.key}</span>
                <span className='bioValue'>{bio.value}</span>
              </div>
            )
          })}
          <div className='link_button'>
            <a href='https://drive.google.com/file/d/1H_bZKIV1MBd1i41WqCeh4u0HGkCO41_G/view?usp=sharing' target ="blank_" >
              Lien vers mon curriculum vitae
            </a>
          </div>
        </motion.div>
        <motion.div className="about_right"
          initial={{ x: 0, opacity: 0 }}
          whileInView={{ x: [250, 0], opacity: 1 }}
          transition={{ duration: 1 }}
        >

          <p> Je suis ravi de vous présenter mon portfolio réalisé et maintenu à jour dans le cadre de mon BUT Informatique à l'IUT de Lille.</p>
          
          <p> Je suis à la recherche d'une alternance en tant que développeur web front-end ou fullstack pour ma 3ème année, où je pourrai développer mes compétences en informatique tout en apportant une valeur ajoutée à votre entreprise.</p>

           <p> J'ai eu l'occasion de collaborer sur divers projets stimulants en équipe que vous pouvez retrouver dans la rubrique : <a class ="linkProject" href="#projets">Mes Projets</a>.</p>

           <p> L'informatique est devenu une réelle passion depuis ma réorientation professionnelle, mais j'ai également d'autres centres d'intérêt très important à mes yeux, parmis lesquels il y a : </p>
            <ul>
              <li>Le sport</li>
              <li>Les manga et l'animation japonaise</li>
              <li>La musique</li>
              <li>Passer du temps avec mes amis</li>
            </ul>
            <p>Fort de mes compétences, de ma passion pour l'informatique et de ma volonté inébranlable d'apprendre, je suis convaincu que je serai en mesure de répondre à vos  attentes au cours d'une alternance !</p>
        </motion.div>

      </div>

    </div>
  )
}

export default About