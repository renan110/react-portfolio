import React, { useEffect, useState } from 'react';
import "./Portfolio.scss"
import { workNavs } from "../../../Data";
import { workImages } from '../../../Data';
import { FiEye, FiGitlab} from "react-icons/fi"
import { DiFirefox } from "react-icons/di"
import { motion } from 'framer-motion';
import { useMediaQuery } from 'react-responsive'

const Portfolio = () => {
  const [tab, setTab] = useState({ name: "Tous les projets" });
  const [works,setWorks] = useState([])
  const [active, setActive] = useState(0);

  useEffect(() => {
    if (tab.name === "Tous les projets") {
      setWorks(workImages)
    } else {
      const newWork = workImages.filter(workImage => {
        return workImage.category === tab.name;
      })
      setWorks(newWork)
    }
  }, [tab])

  const activeTab = (e,index) => {
    setTab({ name: e.target.textContent });
    setActive(index)
  }

  const [isTapped, setIsTapped] = useState(false);
  const isMobile = useMediaQuery({ maxWidth: 767 });

  return (
    <div className="container" id="projets">
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="title"

      >
            <span>Mes projets</span>
            <h1>Principaux projets</h1>
      </motion.div>
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="buttons"
      >
        {workNavs.map((workNav ,index) => {
          return (
            <button
              onClick={(e) => activeTab(e, index)}
              className={`${active === index ? "active" : ""}`}
              key={index}>{workNav}</button>
          )
        })}
      </motion.div>
      <motion.div
        initial={{x: 0 ,opacity: 0}}
          whileInView={{ x: [-250,0], opacity: 1 }}
        transition={{ duration: 1 }}
        exit={{opacity: 0, y: -50}}
        className="workImages"
      >
        {works.map((work) => {
          return (
            <div className="workImage"
             key={work.id}
            >
              <div className='banner'>
                <div className='banner_project_name'>{work.name}</div>
                <div className='banner_project_langages'>{work.langages}</div>
              </div>
                <img src={work.img} alt="workImg" />
              <motion.div
                initial={{opacity: 0}}
                whileHover={{ opacity: [0, 1] }}
                // transition={{duration: 0.3 , ease: "easeInOut"}}
                className='hoverLayer'
                whileTap={{ opacity : 1 }}
                transition={{duration: 0.3 , ease: "easeInOut"}}

              >

                <motion.a href={`${work.link}`} target={`${work.target}`} rel="noreferrer noopener"
                  style={{display:`${work.displayGitIcon}`}}
                  whileInView={{scale: [0,1]}}
                  whileHover={{scale: [1, 1.1]}}
                  transition={{duration: 0.3}}
                >
                  <FiGitlab />
                </motion.a>
                <motion.a href={`${work.site}`} target={`${work.target}`} rel="noreferrer noopener"
                   style={{display:`${work.displayFirefoxIcon}`}}
                   whileInView={{scale: [0,1]}}
                   whileHover={{scale: [1, 1.1]}}
                  transition={{duration: 0.3}}
                >
                  <DiFirefox />
                </motion.a>
                {/* <p>Bientôt disponible</p>

                <motion.a href='#projets' target="_blank" rel="noreferrer noopener"
                whileInView={{scale: [0,1]}}
                 whileHover={{scale: [1, 1.1]}}
                 transition={{duration: 0.3}}
                >
                  <FiEye />
                </motion.a> */}
              </motion.div>
            </div>
          )
        })}
      </motion.div>

    </div>
  )
}

export default Portfolio