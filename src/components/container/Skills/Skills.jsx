import React, { useState } from 'react'
import "./Skills.scss";
import { icons } from '../../../Data';
import { langages } from '../../../Data';
import { transversales } from '../../../Data';
import { motion } from 'framer-motion';
const Skills = () => {
  const [active, setActive] = useState(1)
  return (
    <div className="container" id="competences">
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}  
        className="title"
      >
        <span>Mes compétences</span>
        <h1>Techniques et transversales </h1>
      </motion.div>
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="select"
      >
        <button
          onClick={() => setActive(1)}
          className={active === 1 ? "active" : ""}>Techniques</button>
        <button
          onClick={() => setActive(2)}
          className={active === 2 ? "active" : ""}
        >transversales</button>
      </motion.div>
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="competences"
      >
        {active === 1 && langages.map((langages, index) => {
          return (
            <div key={index} className="tools" >
              <div className='langages_icon icon'>{langages.icon}</div>
              <div className='langages_label'>{langages.label}</div>  
            </div>
          )
        })}
      </motion.div>
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="transversales"
      >
        {active === 2 && transversales.map(transversale => {
          return (
            <div className="transversale" key={transversale.id}>
              <div className="position">
                <div className='transversales_icon icon'>{transversale.icon}</div>
                <div>{transversale.transversale}</div>
              </div>
            </div>
          )
        })}
        </motion.div>
    </div>
  )
}

export default Skills