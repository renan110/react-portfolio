import React, { useState } from 'react'
import "./Career.scss";
import { formations } from '../../../Data';
import { experiences } from '../../../Data';
import { motion } from 'framer-motion';
const Career = () => {
  const [active, setActive] = useState(1)
  return (
    <div className="container" id="parcours">
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}  
        className="title"
      >
        <span>Mon parcours</span>
        <h1>Formations et experiences professionnelles</h1>
      </motion.div>
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="select"
      >
        <button onClick={() => setActive(1)} className={active === 1 ? "active" : ""}>Formations</button>
        <button onClick={() => setActive(2)} className={active === 2 ? "active" : ""}>Experiences pro</button>
      </motion.div>
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="formations"
      >
        {active === 1 && formations.map((formation, index) => {
          return (
            <div key={index} className="tools" >
              <div className='formations_year'>{formation.year}</div>
                <div className='form_label_location'>
                  <div className='formations_label'>{formation.label}</div>  
                  <div className='formations_location'>{formation.location}</div>
                </div>
              </div>
          )
        })}
      </motion.div>
      <motion.div
        initial={{opacity: 0}}
        whileInView={{y: [-50, 0], opacity: 1}}
        className="experiences"
      >
        {active === 2 && experiences.map(experience => {
          return (
            <div className="experience" key={experience.id}>
              <div className='experiences_year'>{experience.year}</div>
                <div className='form_label_location'>
                  <div className='experiences_label'>{experience.label}</div>  
                  <div className='experiences_info'>{experience.informations}</div>  
                  <div className='experiences_techno'>{experience.technologies}</div> 
                  <div className='experiences_location'>{experience.location}</div>
                </div>
            </div>
          )
        })}
        </motion.div>
    </div>
  )
}

export default Career